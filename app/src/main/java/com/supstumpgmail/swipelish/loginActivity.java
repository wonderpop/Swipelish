package com.supstumpgmail.swipelish;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class loginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }

    public  void onOkButtonClick(View view){
        Intent intent = new Intent();
        String name = ((EditText)findViewById(R.id.etName)).getText().toString();
        if (name.length() == 0) {
            Toast.makeText(this,"Введите имя",Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this,name,Toast.LENGTH_LONG).show();
            intent.putExtra("name", name);
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            return true;
        }
        return false;
    }
}


