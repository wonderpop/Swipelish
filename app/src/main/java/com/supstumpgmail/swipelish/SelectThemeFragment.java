package com.supstumpgmail.swipelish;

        import android.app.Activity;
        import android.app.Fragment;
        import android.content.Context;
        import android.content.res.TypedArray;
        import android.database.Cursor;
        import android.database.SQLException;
        import android.database.sqlite.SQLiteDatabase;
        import android.graphics.Color;
        import android.graphics.drawable.BitmapDrawable;
        import android.graphics.drawable.Drawable;
        import android.net.Uri;
        import android.nfc.Tag;
        import android.os.Bundle;
        import android.support.constraint.ConstraintLayout;
        import android.support.design.widget.Snackbar;
        import android.util.Log;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.view.animation.Animation;
        import android.view.animation.AnimationUtils;
        import android.widget.Button;
        import android.widget.ImageButton;
        import android.widget.ImageView;
        import android.widget.LinearLayout;
        import android.widget.RelativeLayout;
        import android.widget.Toast;

        import java.io.IOException;
        import java.lang.reflect.Array;
        import java.util.ArrayList;
        import java.util.function.ToDoubleBiFunction;

public class SelectThemeFragment extends Fragment {

    Context context;
    LinearLayout llMain;
    int count =0;

    private DataBaseManager mDBHelper;
    private SQLiteDatabase mDb;

    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
        context=activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_select_theme, container, false);

        dbInit(rootView);
        btnCreate(rootView);

        return rootView;
    }

    private void dbInit(View v){
        //db
        mDBHelper = new DataBaseManager(v.getContext());

        try {
            mDBHelper.updateDataBase();
        } catch (IOException mIOException) {
            throw new Error("UnableToUpdateDatabase");
        }

        try {
            mDb = mDBHelper.getWritableDatabase();
        } catch (SQLException mSQLException) {
            throw mSQLException;
        }
    }

    private void btnCreate(View v){
        llMain = (LinearLayout) v.findViewById(R.id.select_linear);

        int[] attrs = new int[]{R.attr.selectableItemBackground};
        TypedArray typedArray = context.obtainStyledAttributes(attrs);
        int backgroundResource = typedArray.getResourceId(0, 0);
        final MainActivity main = (MainActivity ) getActivity();

        Button.OnClickListener btnOnClick = new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (main.dupa == 2){
                    main.lupa = v.getId();
                    main.createVocabularyFragment();
                } else {
                    main.lupa = v.getId();
                    main.createLearnActivity();
                }
            }
        };


        LinearLayout.LayoutParams lParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, 360);

        lParams.setMargins(0,0,0,24);

        Cursor cursor = mDb.rawQuery("SELECT * FROM vocab", null);
        count = cursor.getCount();
        cursor.moveToFirst();

        for (int i = 0; i<count;i++){
            ImageButton btnNew = new ImageButton(v.getContext());
            btnNew.setImageResource(backgroundResource);
            btnNew.setPadding(0,0,0,0);
            btnNew.setScaleType(ImageView.ScaleType.FIT_CENTER);
            btnNew.setOnClickListener(btnOnClick);
            btnNew.setId(i);

            btnNew.setBackgroundResource(getResources().getIdentifier(cursor.getString(1), "drawable",getActivity().getPackageName()));
            cursor.moveToNext();
            llMain.addView(btnNew, lParams);
        }
        cursor.close();
        typedArray.recycle();
    }

}
