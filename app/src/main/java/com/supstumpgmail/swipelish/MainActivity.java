package com.supstumpgmail.swipelish;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.content.pm.ActivityInfo;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.text.Layout;
import android.transition.Fade;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.Random;

import static android.app.PendingIntent.getActivity;
import static android.service.autofill.Validators.and;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    int userLvl = 0;
    int userExp = 0;
    int dupa = 0;
    int lupa = 0;
    String userName = "0";

    //fragments
    FragmentTransaction fTrans;
    SelectThemeFragment frg1;
    VocabularyWords frg2;
    WordFragment wfrg;
    PageFragment pfrg;


    //preferences
    SharedPreferences sPref;

    //DB
    private DataBaseManager mDBHelper;
    public SQLiteDatabase mDb;

    ConstraintLayout llMain;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        frg1 = new SelectThemeFragment();
        frg2 = new VocabularyWords();
        wfrg = new WordFragment();
        pfrg = new PageFragment();

        llMain = (ConstraintLayout) findViewById(R.id.mainMenuLayout);

        //db
        mDBHelper = new DataBaseManager(this);

        try {
            mDBHelper.updateDataBase();
        } catch (IOException mIOException) {
            throw new Error("UnableToUpdateDatabase");
        }

        try {
            mDb = mDBHelper.getWritableDatabase();
        } catch (SQLException mSQLException) {
            throw mSQLException;
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onDestroy(){
        save();
        super.onDestroy();

    }

    @Override
    public void onPause(){
        super.onPause();
        save();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        ProgressBar progressBar = (ProgressBar) findViewById(R.id.lvl_progressbar);
        if (progressBar != null) {
            load();
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_main_menu) {
            FragmentTransaction fTrans = getFragmentManager().beginTransaction();
            fTrans.remove(frg1);
            fTrans.commit();
        } else if (id == R.id.nav_vocabulary) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    void save(){
        sPref = getPreferences(MODE_PRIVATE);
        Editor ed = sPref.edit();
        ed.putInt("user_lvl", userLvl);
        ed.putInt("user_exp", userExp);
        ed.putString("user_name", userName);
        ed.commit();
    }

    void load() {
        sPref = getPreferences(MODE_PRIVATE);
        userLvl = sPref.getInt("user_lvl", 0);
        userExp = sPref.getInt("user_exp", 0);
        userName = sPref.getString("user_name","0");

        if (userLvl >= 0){
            setLvl(userLvl, userExp);
        }
    }



    public void letsLernButtonOnClick(View view) {
        dupa = 1;

        FragmentTransaction fTrans = getFragmentManager().beginTransaction();
        fTrans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fTrans.add(R.id.frgmCont, frg1);
        fTrans.addToBackStack(null);
        fTrans.commit();
        addExp(10);
    }

    public void testButtonOnClick(View view){


    }

    public void vocabularyOnClick(View view) {
        dupa = 2;

        FragmentTransaction fTrans = getFragmentManager().beginTransaction();
        fTrans.add(R.id.frgmCont, frg1);
        fTrans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fTrans.addToBackStack(null);
        fTrans.commit();

//        Cursor cursor = mDb.rawQuery("SELECT * FROM vocab", null);
//        cursor.moveToFirst();
//
//        cursor.close();
//
//        Snackbar.make(view, product, Snackbar.LENGTH_LONG)
//                .setAction("Action", null).show();
//
//        LinearLayout.LayoutParams lParams = new LinearLayout.LayoutParams(
//                LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//        Button btnNew = new Button(this);
//        btnNew.setText("Hui");
//        llMain.addView(btnNew, lParams);
    }

    public void basicOnClick(View view){
    }

    public void addExp(int exp){
        ProgressBar progressBar = findViewById(R.id.lvl_progressbar);
        TextView lvlExp = findViewById(R.id.lvl_exp_text);

        userExp+= exp;

        progressBar.setProgress(progressBar.getProgress()+exp);
        lvlExp.setText(Integer.toString(progressBar.getProgress())+"/"+Integer.toString(progressBar.getMax()));
        if (progressBar.getProgress() == progressBar.getMax()){
            lvlUp();
        }
    }

    public void lvlUp(){
        ProgressBar progressBar = findViewById(R.id.lvl_progressbar);
        TextView lvl = findViewById(R.id.lvl_text);
        TextView nextLvl = findViewById(R.id.next_lvl_text);
        TextView lvlExp = findViewById(R.id.lvl_exp_text);

        userExp = 0;
        userLvl++;

        lvl.setText("lvl "+Integer.toString(userLvl));
        nextLvl.setText("lvl "+Integer.toString(userLvl+1));
        progressBar.setMax(progressBar.getMax()+100);
        progressBar.setProgress(0);
        lvlExp.setText("0/"+Integer.toString(progressBar.getMax()));

    }

    public void setLvl(int ulvl, int exp){
        ProgressBar progressBar = findViewById(R.id.lvl_progressbar);
        TextView lvl = findViewById(R.id.lvl_text);
        TextView nextLvl = findViewById(R.id.next_lvl_text);
        TextView lvlExp = findViewById(R.id.lvl_exp_text);
        TextView name = findViewById(R.id.user_name_text);

        progressBar.setProgress(exp);
        progressBar.setMax(200+ulvl*100);

        lvl.setText("lvl "+Integer.toString(ulvl));
        nextLvl.setText("lvl "+Integer.toString(ulvl+1));
        lvlExp.setText(Integer.toString(exp)+"/"+Integer.toString(progressBar.getMax()));
        if (userName == "0") {
            Intent intent = new Intent(this, loginActivity.class);
            startActivityForResult(intent, 1);
        } else {
            name.setText(userName);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null) {return;}
        TextView tvName = findViewById(R.id.user_name_text);
        String name = data.getStringExtra("name");
        userName = name;
        tvName.setText(userName);
    }

    public void createVocabularyFragment(){
        FragmentTransaction fTrans = getFragmentManager().beginTransaction();
        fTrans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fTrans.add(R.id.frgmCont, frg2);
        fTrans.addToBackStack(null);
        fTrans.commit();
    }

    public void createLearnActivity(){
        Intent intent = new Intent(MainActivity.this, ActivityTest.class);
        startActivity(intent);
    }

    public void sadOnClick(View view){
        if (wfrg.study == 0)
            Toast.makeText(this, "Вы не изучили это слово :c", Toast.LENGTH_SHORT).show();
    }
}
