package com.supstumpgmail.swipelish;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class VocabularyWords extends Fragment {
    String[] words;
    WordFragment frg;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_vocabulary_words, container, false);
        frg = new WordFragment();
        ListView lvMain = (ListView) rootView.findViewById(R.id.words_list);
        final MainActivity main = (MainActivity) getActivity();
        getWords(rootView);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(rootView.getContext(),
                android.R.layout.simple_list_item_1, words);
        lvMain.setAdapter(adapter);
        lvMain.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                view.setTag(words[position]);
                FragmentTransaction fTrans = getFragmentManager().beginTransaction();
                fTrans.add(R.id.frgmCont, frg);
                fTrans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                fTrans.addToBackStack(null);
                fTrans.commit();
                String x ="'"+view.getTag().toString()+"'";
                Cursor cursor = main.mDb.rawQuery("SELECT * FROM Words WHERE word ="+x+";", null);
                cursor.moveToFirst();
                String name =(cursor.getString(2));
                String transcription =(cursor.getString(4));
                String translate =(cursor.getString(3));
                int study =(cursor.getInt(5));
                frg.name = name;
                frg.transcription = transcription;
                frg.translate = translate;
                frg.study = study;
                cursor.close();
            }
        });
        return rootView;
    }

    private void getWords(View v){
        int count;
        final MainActivity main = (MainActivity ) getActivity();
        main.lupa++;
        Cursor cursor = main.mDb.rawQuery("SELECT * FROM Words WHERE id_deck ="+main.lupa+";", null);
        count = cursor.getCount();
        words = new String[]{""};
        words = new String[count];
        cursor.moveToFirst();
        for (int i = 0; i<count; i++){
            words[i]= cursor.getString(2);
            cursor.moveToNext();
        }
        cursor.close();
    }
}

