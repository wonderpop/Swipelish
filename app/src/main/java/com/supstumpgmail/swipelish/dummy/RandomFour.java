package com.supstumpgmail.swipelish.dummy;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;

public class RandomFour
{
    private final HashSet<Integer> input = new HashSet<Integer>();
    private final Random rnd;
    private final int Count;
    private int genCount=0;
    public RandomFour(int in)
    {
        Count=in;
        rnd=new Random(in);
    }

    public int generate()
    {
        if (genCount>=Count)
            return -1;
        int next;
        do
        {
            next = rnd.nextInt(Count);
        }
        while (!input.add(next));
        genCount++;
        return next;
    }
}
