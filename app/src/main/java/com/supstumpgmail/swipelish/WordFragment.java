package com.supstumpgmail.swipelish;

import android.app.Fragment;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class WordFragment extends Fragment {
    String name;
    String transcription;
    String translate;
    int study;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.word_card, container, false);
        setContent(rootView);
        return rootView;
    }

    private void setContent(View view){
        final TextView tvName = (TextView) view.findViewById(R.id.tv_word_name);
        TextView tvTranscription = (TextView) view.findViewById(R.id.tv_word_transcription);
        TextView tvTranslate = (TextView) view.findViewById(R.id.tv_word_translate);
        ImageView ivMedal = (ImageView) view.findViewById(R.id.iv_word_medal);
        ImageView ivWord = (ImageView) view.findViewById(R.id.iv_word_image);

        tvName.setText(name);
        name = name.replaceAll("\\s+","_");
        name = name.replace("-","");
        name = name.replace("(","");
        name = name.replace(")","");
        tvTranscription.setText("["+transcription+"]");
        tvTranslate.setText(translate);
        ivWord.setBackgroundResource(getResources().getIdentifier(name.toLowerCase(), "drawable",getActivity().getPackageName()));

        switch(study) {
            case 0:
                ivMedal.setImageResource(R.drawable.sad);
                break;
            case 1:
                ivMedal.setImageResource(R.drawable.first);
                break;
            case 2:
                ivMedal.setImageResource(R.drawable.second);
                break;
            case 3:
                ivMedal.setImageResource(R.drawable.third);
                break;
        }
    }
}

