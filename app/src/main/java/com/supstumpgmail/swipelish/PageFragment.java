package com.supstumpgmail.swipelish;

import java.util.Random;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class PageFragment extends Fragment {

    static final String TAG = "myLogs";

    static final String ARGUMENT_PAGE_NUMBER = "arg_page_number";
    static final String SAVE_PAGE_NUMBER = "save_page_number";

    int pageNumber;
    int backColor;

    static PageFragment newInstance(int page) {
        PageFragment pageFragment = new PageFragment();
        Bundle arguments = new Bundle();
        arguments.putInt(ARGUMENT_PAGE_NUMBER, page);
        pageFragment.setArguments(arguments);
        return pageFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pageNumber = getArguments().getInt(ARGUMENT_PAGE_NUMBER);
        Log.d(TAG, "onCreate: " + pageNumber);

        Random rnd = new Random();
        backColor = Color.argb(40, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));

        int savedPageNumber = -1;
        if (savedInstanceState != null) {
            savedPageNumber = savedInstanceState.getInt(SAVE_PAGE_NUMBER);
        }
        Log.d(TAG, "savedPageNumber = " + savedPageNumber);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                         Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_vocabulary_words, container, false);
        View view = inflater.inflate(R.layout.fragment_learn, null);
//        final ActivityLearn main = (ActivityLearn) getActivity();
//        TextView tvWord = (TextView) view.findViewById(R.id.tv_test_word);
//        TextView tvVar1 = (TextView) view.findViewById(R.id.tv_test_var1);
//        TextView tvVar2 = (TextView) view.findViewById(R.id.tv_test_var2);
//        TextView tvVar3 = (TextView) view.findViewById(R.id.tv_test_var3);
//        TextView tvVar4 = (TextView) view.findViewById(R.id.tv_test_var4);
////        String x ="'2'";
////        Cursor cursor = main.mDb.rawQuery("SELECT * FROM Words WHERE id_deck ="+x+";", null);
////        for (int i=1; i<10; i++){
////            cursor.moveToFirst();
////            RandomFour z = new RandomFour(32);
////            int f = z.generate();
////            for(int j = 1; j<f; j++){
////                cursor.moveToNext();
////            }
////            word = cursor.getString(2);
////            cursor.moveToFirst();
////        }
////        cursor.close();
//        tvWord.setText(word);
//        tvVar1.setText(translate1);
//        tvVar2.setText(translate2);
//        tvVar3.setText(translate3);
//        tvVar4.setText(translate4);
//
//
//
        TextView tvPage = (TextView) view.findViewById(R.id.tvPage);
        tvPage.setText("Page " + pageNumber);
        tvPage.setBackgroundColor(backColor);

        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(SAVE_PAGE_NUMBER, pageNumber);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy: " + pageNumber);
    }
}
